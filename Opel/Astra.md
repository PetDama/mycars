Numărul de uși: 5 
Număr de locuri: 5 
Ampatament:	2614,00 mm 
            8,58 ft
            102,91 inchi
            2,6140 m 

Calea din față:	1488,00 mm 
                4,88 ft
                58.58 in
                1,4880 m 

Calea din spate:	1488,00 mm 
                    4,88 ft
                    58.58 in
                    1,4880 m (metri)

Lungime:	4249,00 mm 
            13,94 ft
            167,28 inchi
            4,2490 m

Lăţime:	1753,00 mm 
        5,75 ft
        69.02 in
        1,7530 m 

Înălţime: 1467,00 mm 
        4,81 ft
        57.76 in
        1,4670 m 

Curatenie totala:   160,00 mm 
                    0,52 ft
                    6.30 in
                    0,1600 m 

Volumul minim al portbagajului:	380,0 l 
                                13.42ft3 
                                0,38 mc 
                                380000,00 cmc 

Volumul maxim al portbagajului:	1330,0 l 
                                6.97ft3 
                                1,33 mc 
                                1330000,00 cmc 

Greutate redusă:	1165 kg 
                    2568,39 lire sterline

Greutate maximă: 1740 kg 
                3836,04 lire sterline

Volum rezervor de combustibil:	52,0 l
                                11,44 imp gal 
                                ora 13.74.gal